from sqlalchemy import (Table, Column, Integer, String, ForeignKey, Boolean,
                        ForeignKeyConstraint, TIMESTAMP, func, select)
from sqlalchemy.orm import relationship, backref
from database import Base
from datetime import datetime


watchers = Table(
    'watchers',
    Base.metadata,
    Column('users_id', Integer, ForeignKey('users.id')),
    Column('vk_users_uid', Integer, ForeignKey('vk_users.uid')),
)


shown_likes = Table(
    'shown_likes',
    Base.metadata,
    Column('users_id', Integer, ForeignKey('users.id')),
    Column('likes_post_id', Integer, nullable=False),
    Column('likes_vk_user_uid', Integer, nullable=False),
    ForeignKeyConstraint(
        ['likes_post_id', 'likes_vk_user_uid'],
        ['likes.post_id', 'likes.vk_users_uid'],
        ondelete='cascade',
    ),
)


shown_vk_friendship = Table(
    'shown_vk_friendship',
    Base.metadata,
    Column('users_id', Integer, ForeignKey('users.id')),
    Column('vk_friendship_left_uid', Integer, nullable=False),
    Column('vk_friendship_right_uid', Integer, nullable=False),
    ForeignKeyConstraint(
        ['vk_friendship_left_uid', 'vk_friendship_right_uid'],
        ['vk_friendship.left_uid', 'vk_friendship.right_uid'],
        ondelete='cascade',
    ),
)


class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=False)
    first_name = Column(String(50))
    last_name = Column(String(50))
    username = Column(String(50))
    registration_timestamp = Column(
        Integer,
        default=int(datetime.utcnow().timestamp()),   # TODO: Timestamp
    )
    deactivation_timestamp = Column(Integer, default=None)
    query = relationship('Query', uselist=False, back_populates='user')
    watched_vk_users = relationship(
        'VKUsers',
        secondary=watchers,
        backref=backref('watchers', lazy='dynamic'),
        lazy='dynamic',
    )

    def is_active(self):
        return not self.deactivation_timestamp

    def activate(self):
        self.deactivation_timestamp = None
        return self

    def deactivate(self):
        self.deactivation_timestamp = int(datetime.utcnow().timestamp())
        return self

    def watch(self, vk_user):
        if not self.is_watching(vk_user):
            self.watched_vk_users.append(vk_user)
            return self

    def unwatched(self, vk_user):
        if self.is_watching(vk_user):
            self.watched_vk_users.remove(vk_user)
            return self

    def is_watching(self, vk_user):
        return self.watched_vk_users.filter(
            watchers.c.vk_users_uid == vk_user.uid).count() > 0


class VKUsers(Base):
    __tablename__ = 'vk_users'
    uid = Column(Integer, primary_key=True)
    first_name = Column(String(50))
    last_name = Column(String(50))
    last_check_likes_timestamp = Column(TIMESTAMP, default=None)
    watching = relationship('Watching', back_populates='vk_user')
    friends = relationship(
        'VKUsers',
        secondary='vk_friendship',
        primaryjoin='VKUsers.uid == vk_friendship.c.left_uid',
        secondaryjoin='VKUsers.uid == vk_friendship.c.right_uid',
    )
    all_friendship = relationship(
        'VKFriendship',
        primaryjoin='or_(VKUsers.uid==vk_friendship.c.left_uid, '
                    'VKUsers.uid==vk_friendship.c.right_uid)'
    )

    def __repr__(self):
        return '<VKUsers> {}'.format(self.uid)

    @property
    def full_name(self):
        full_name = '{} {}'.format(self.first_name, self.last_name)
        return full_name if full_name.strip() != '' else self.uid

    @property
    def vk_page_link(self):
        return 'https://vk.com/id{}'.format(self.uid)


class Watching(Base):
    __tablename__ = 'watching'
    id = Column(Integer, primary_key=True)
    uid = Column(Integer, ForeignKey('vk_users.uid'))
    vk_user = relationship('VKUsers', back_populates='watching')
    online = Column(Boolean, default=False)
    last_seen_timestamp = Column(Integer)
    platform = Column(Integer)
    timestamp = Column(Integer)


class Posts(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    link = Column(String(50), nullable=False, unique=True)
    liked_vk_users = relationship('VKUsers', secondary='likes')


class Likes(Base):
    __tablename__ = 'likes'
    post_id = Column(
        Integer,
        ForeignKey('posts.id', ondelete='cascade'),
        primary_key=True,
    )
    vk_users_uid = Column(Integer, ForeignKey('vk_users.uid'), primary_key=True)
    timestamp = Column(TIMESTAMP, server_default=func.now())
    vk_user = relationship('VKUsers', backref='likes')
    post = relationship('Posts', backref='likes')
    shown = relationship(
        'Users',
        secondary='shown_likes',
        backref='shown_likes',
        primaryjoin='and_(Likes.post_id==shown_likes.c.likes_post_id, '
                    'Likes.vk_users_uid==shown_likes.c.likes_vk_user_uid)',
        secondaryjoin='Users.id == shown_likes.c.users_id',
    )


class VKFriendship(Base):
    __tablename__ = 'vk_friendship'
    left_uid = Column(Integer, ForeignKey('vk_users.uid'), primary_key=True)
    right_uid = Column(Integer, ForeignKey('vk_users.uid'), primary_key=True)
    timestamp = Column(TIMESTAMP, server_default=func.now())
    left_vk_user = relationship(
        'VKUsers',
        primaryjoin=left_uid == VKUsers.uid,
    )
    right_vk_user = relationship(
        'VKUsers',
        primaryjoin=right_uid == VKUsers.uid,
    )
    shown = relationship(
        'Users',
        secondary='shown_vk_friendship',
        backref='shown_vk_friendship',
        primaryjoin='and_(VKFriendship.left_uid=='
                    'shown_vk_friendship.c.vk_friendship_left_uid, '
                    'VKFriendship.right_uid=='
                    'shown_vk_friendship.c.vk_friendship_right_uid)',
        secondaryjoin='Users.id == shown_vk_friendship.c.users_id',
    )

    def __repr__(self):
        return '{} {} {}'.format(self.left_uid, self.right_uid, self.timestamp)

    def get_friend_for_uid(self, for_uid):
        return (self.right_vk_user if self.right_uid != for_uid
                else self.left_vk_user)


class Query(Base):
    __tablename__ = 'query'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), unique=True)
    user = relationship('Users', back_populates='query')
    create_timestamp = Column(
        Integer,
        default=int(datetime.utcnow().timestamp()),   # TODO: Timestamp
    )
    type_query = Column(
        String(30),
        nullable=False,
        default='select_type_requested_data',
    )
    vk_user_uid = Column(Integer)


vk_friendship_union = select(
    [
        VKFriendship.left_uid,
        VKFriendship.right_uid,
        VKFriendship.timestamp,
    ]
).union(
    select(
        [
            VKFriendship.right_uid,
            VKFriendship.left_uid,
            VKFriendship.timestamp,
        ]
    )
).alias()


VKUsers.all_friends = relationship(
    'VKUsers',
    secondary=vk_friendship_union,
    primaryjoin=VKUsers.uid == vk_friendship_union.c.right_uid,
    secondaryjoin=VKUsers.uid == vk_friendship_union.c.left_uid,
    viewonly=True,
)
