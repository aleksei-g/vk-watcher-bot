import time
from datetime import datetime
import logging
from os import getenv
from threading import Thread
from queue import Queue
import urllib.request
import vk
import db_function

logging.basicConfig(level=getenv("LOG_LEVEL", "WARNING"))
logger = logging.getLogger('{}{}'.format(__name__, '.vk_watcher'))

DEFAULT_LANGUAGE_API_VK = 'ru'
DEFAULT_TIMEOUT_API_VK = 60
DEFAULT_NEWS_RELEVANCE_IN_SEC = 60 * 60 * 24 * 2
DEFAULT_MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED = 10
DEFAULT_DELAY_BETWEEN_VK_USERS_LIKES_CHECKS = 600
DEFAULT_NUMBER_OF_THREADS = 1
DELAY_BETWEEN_REQUESTS = 0.34
DELAY_AFTER_TOO_MANY_REQUEST = 5
DELAY_BETWEEN_STATUS_VK_USERS_CHECKS = 60
COUNT_OF_ATTEMPTS_IN_TPY_API_METHOD = 3
fmt_str = '%a, %d %b %H:%M:%S'
PREFIX_SOURCE_IDS = {'users': 'u', 'groups': 'g'}
API_VERSION_VK = '5.92'
APP_ID_VK = getenv('APP_ID_VK')
APP_TOKEN_VK = getenv('APP_TOKEN_VK')
NUMBER_OF_THREADS = int(getenv(
    'NUMBER_OF_THREADS', DEFAULT_NUMBER_OF_THREADS))
NEWS_RELEVANCE_IN_SEC = int(getenv(
    'NEWS_RELEVANCE_IN_SEC', DEFAULT_NEWS_RELEVANCE_IN_SEC))
LANGUAGE_API_VK = getenv('LANGUAGE_API_VK', DEFAULT_LANGUAGE_API_VK)
MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED = int(getenv(
    'MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED',
    DEFAULT_MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED,
))
DELAY_BETWEEN_VK_USERS_LIKES_CHECKS = int(getenv(
    'DELAY_BETWEEN_VK_USERS_LIKES_CHECKS',
    DEFAULT_DELAY_BETWEEN_VK_USERS_LIKES_CHECKS,
))
HEROKU_URL = getenv('HEROKU_URL')


class VKAPIThread(Thread):
    def __init__(self, queue, *args, **kwargs):
        Thread.__init__(self, *args, **kwargs)
        self.queue = queue

    def run(self):
        while True:
            task = self.queue.get()
            logger.info(
                '{}: run task: func: {}, params: {}'.format(
                    datetime.now(),
                    task.get('func').__name__,
                    task.get('params'),
                ))
            self.run_task(task)
            logger.info(
                '{}: task done: func: {}, params: {}'.format(
                    datetime.now(),
                    task.get('func').__name__,
                    task.get('params'),
                ))
            self.queue.task_done()
            logger.info(
                '{}: sleep {} sec. after task: func: {}, params: {}'.format(
                    datetime.now(),
                    DELAY_BETWEEN_REQUESTS,
                    task.get('func').__name__,
                    task.get('params')),
            )
            time.sleep(DELAY_BETWEEN_REQUESTS)

    def run_task(self, task):
        task.get('func')(task.get('params'))


def threads_initialize(threads=None, number_of_threads=NUMBER_OF_THREADS):
    threads = threads or []
    for i in range(number_of_threads):
        thread = VKAPIThread(get_vk_liked_post_queue)
        thread.setDaemon(True)
        thread.start()
        threads.append(thread)
    return threads


while True:
    try:
        session = vk.Session(access_token=APP_TOKEN_VK)
        api = vk.API(
            session,
            v=API_VERSION_VK,
            lang=LANGUAGE_API_VK,
            timeout=DEFAULT_TIMEOUT_API_VK,
        )
        break
    except Exception:
        time.sleep(5)


get_vk_liked_post_queue = Queue()
threads = threads_initialize()


def try_api_method(api_method,
                   param,
                   count_attempts=COUNT_OF_ATTEMPTS_IN_TPY_API_METHOD):
    result = {'result': None, 'received_error': None}
    need_to_repeat = True
    attempt = 0
    while need_to_repeat and attempt < count_attempts:
        need_to_repeat = False
        attempt += 1
        try:
            result['result'] = api_method(**param)
            time.sleep(DELAY_BETWEEN_REQUESTS)
        except vk.exceptions.VkAPIError as e:
            result['received_error'] = e
            logger.error(e)
            if e.code == 6:
                need_to_repeat = True
                time.sleep(DELAY_AFTER_TOO_MANY_REQUEST)
        except Exception as e:
            result['received_error'] = e
            logger.error(e)
            need_to_repeat = True
    return result


def get_status_vk_users(vk_user_uids):
    params = {
        'user_ids': vk_user_uids,
        'fields': 'online, last_seen, timezone',
    }
    return try_api_method(api.users.get, params).get('result') or []


def get_friends_vk_user(vk_user_uid):
    params = {
        'user_id': vk_user_uid,
        'fields': 'domain',
    }
    return (try_api_method(api.friends.get, params).
            get('result', {}).get('items', []))


def check_vk_user_updates(vk_user):
    friends = get_friends_vk_user(vk_user.uid)
    db_function.update_friends_vk_user(vk_user, friends)
    task = {'func': add_vk_liked_posts, 'params': vk_user.uid}
    db_function.update_timestamp_last_check_likes_for_vk_user(
        vk_user.uid)
    get_vk_liked_post_queue.put(task)
    logger.info(
        '{}: add task: func: {}, params: {}'.format(
            datetime.now(),
            task.get('func').__name__,
            task.get('params')))


def add_status_vk_users(vk_user_uids):
    status_users = {}
    users_info = get_status_vk_users(vk_user_uids)
    for user_info in users_info:
        logger.info(
            '{}: user {}: user_info: {}'.format(
                datetime.now(), user_info.get('id'), user_info))
        if user_info.get('deactivated') is None:
            watching = db_function.add_watching(user_info)
            vk_user_in_db = watching['watching'].vk_user
            status_users[user_info.get('id')] = watching['watching']
            if user_info.get('can_access_closed') is False:
                continue
            if (not vk_user_in_db.last_check_likes_timestamp or
                    (watching['is_new'] and
                     not watching['watching'].online
                     and (datetime.now().timestamp() -
                          vk_user_in_db.last_check_likes_timestamp.timestamp()
                          >= DELAY_BETWEEN_VK_USERS_LIKES_CHECKS))):
                check_vk_user_updates(vk_user_in_db)
        else:
            status_users[user_info.get('id')] = None
    return status_users


def get_source_ids(vk_user_id):
    subscriptions = try_api_method(
        api.users.getSubscriptions,
        {'user_id': vk_user_id, 'extended': 0},
    ).get('result')
    groups = try_api_method(
        api.groups.get,
        {'user_id': vk_user_id},
    ).get('result')
    users = try_api_method(
        api.friends.get,
        {'user_id': vk_user_id},
    ).get('result')
    source = [
        subscriptions,
        {'groups': groups},
        {'users': users},
    ]
    formatted_ids = []
    for type_id, prefix_id in PREFIX_SOURCE_IDS.items():
        for item in source:
            if item is None:
                continue
            ids_dict = item.get(type_id) or {}
            ids = ids_dict.get('items', [])
            formatted_ids.extend(['{}{}'.format(prefix_id, id) for id in ids])
    formatted_ids = list(set(formatted_ids))
    return formatted_ids


def get_news_feed(source_ids,
                  max_count_of_step=MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED,
                  news_relevance_in_sec=NEWS_RELEVANCE_IN_SEC):
    posts = []
    news_feed = {'next_from': True}
    step = 0
    while step < max_count_of_step and news_feed.get('next_from'):
        params = {
            'filters': 'post',
            'source_ids': ', '.join(source_ids),
            'count': 100,
            'start_from': news_feed.get('next_from'),
            'start_time': int(time.time()) - news_relevance_in_sec,
        }
        news_feed = try_api_method(api.newsfeed.get, params).get('result')
        posts.extend(news_feed.get('items'))
        step += 1
    return posts[::-1]


def get_liked_posts(vk_user_id, posts):
    logger.info(
        '{}: user {}: start search liked post among {} posts'.format(
            datetime.now(), vk_user_id, len(posts)))
    liked_posts = []
    step = 0
    for post in posts:
        params = {
            'user_id': vk_user_id,
            'item_id': post.get('post_id'),
            'type': post.get('type'),
            'owner_id': post.get('source_id'),
        }
        result = try_api_method(api.likes.isLiked, params)
        if result.get('received_error'):
            is_liked = 0
        else:
            is_liked = result.get('result', {}).get('liked')
        if is_liked:
            link_to_post = 'https://vk.com/wall{}_{}'.format(
                post.get('source_id'),
                post.get('post_id'),
            )
            liked_posts.append(link_to_post)
        step += 1
    logger.info(
        '{}: user {}: end search liked post.\nLiked posts: {}'.format(
            datetime.now(), vk_user_id, liked_posts))
    return liked_posts


def add_vk_liked_posts(vk_user_id):
    source_ids = get_source_ids(vk_user_id)
    posts = get_news_feed(source_ids)
    liked_posts = get_liked_posts(vk_user_id, posts)
    for post in liked_posts:
        db_function.add_liked_post(vk_user_id, post)
    db_function.update_timestamp_last_check_likes_for_vk_user(vk_user_id)


def heroku_alarm():
    try:
        urllib.request.urlopen(HEROKU_URL)
    except:
        pass


def watchdog_of_threads(threads_list, number_of_threads=NUMBER_OF_THREADS):
    threads_is_alive = [thread for thread in threads_list if thread.is_alive()]
    number_of_need_to_create_threads = number_of_threads - len(threads_is_alive)
    if number_of_need_to_create_threads > 0:
        threads_list = threads_initialize(
            threads_list,
            number_of_need_to_create_threads,
        )
    return threads_list


if __name__ == '__main__':
    while True:
        logger.info('{}: new step in while'.format(datetime.now()))
        list_watching_vk_users = db_function.get_list_watching_vk_users()
        add_status_vk_users(list_watching_vk_users)
        db_function.delete_old_watching()
        db_function.delete_old_likes_and_posts()
        threads = watchdog_of_threads(threads)
        heroku_alarm()
        logger.info(
            'THREADS: {}'.format(
                ', '.join(['\"{}\" is alive: {}'.format(
                    thread.name, thread.is_alive()) for thread in threads])))
        logger.info('{}: sleep {} sec on step in while'.format(
            datetime.now(), DELAY_BETWEEN_STATUS_VK_USERS_CHECKS))
        time.sleep(DELAY_BETWEEN_STATUS_VK_USERS_CHECKS)
