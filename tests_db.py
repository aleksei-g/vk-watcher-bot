import os
os.environ['DATABASE_URL'] = 'sqlite:///:memory:'
import unittest
from database import session, engine
import db_function
import models


class TestCase(unittest.TestCase):
    def setUp(self):
        db_function.init_db()
        if os.environ['DATABASE_URL'].startswith('sqlite'):
            engine.execute('PRAGMA foreign_keys = ON')
        self.prepare_db()
       
    def tearDown(self):
        session.remove()
        db_function.drop_all()

    def prepare_db(self):
        self.data = {
            'users': [
                models.Users(
                    id=11111,
                    first_name='User1',
                    username='User1',
                    ),
                models.Users(
                    id=22222,
                    first_name='User2',
                    username='User2',
                    ),
            ],
            'vk_users': [
                models.VKUsers(
                    uid=111111,
                    first_name='VK_User1',
                    ),
                models.VKUsers(
                    uid=222222,
                    first_name='VK_User2',
                    ),
                models.VKUsers(
                    uid=333333,
                    first_name='VK_User3',
                ),
                models.VKUsers(
                    uid=444444,
                    first_name='VK_User4',
                ),
            ],
            'posts': [
                models.Posts(link='https://site.com/link1'),
                models.Posts(link='https://site.com/link2'),
            ],
        }
        for model in self.data.values():
            for item in model:
                session.add(item)
            session.commit()
  
    def add_likes_to_db(self):
        post1 = session.query(models.Posts).all()[0]
        post2 = session.query(models.Posts).all()[1]
        vk_user1 = (
                session.query(models.VKUsers).get(self.data['vk_users'][0].uid))
        vk_user2 = (
                session.query(models.VKUsers).get(self.data['vk_users'][1].uid))
        post1.liked_vk_users.append(vk_user1)
        post2.liked_vk_users.append(vk_user1)
        post2.liked_vk_users.append(vk_user2)
        session.add_all([post1, post2])
        session.commit()
        return {
            post1: [vk_user1],
            post2: [vk_user1, vk_user2],
        }

    def add_shown_likes_to_db(self): 
        self.add_likes_to_db()
        like1 = session.query(models.Likes).all()[0]
        like2 = session.query(models.Likes).all()[2]
        user1 = session.query(models.Users).get(self.data['users'][0].id)
        user2 = session.query(models.Users).get(self.data['users'][1].id)
        like1.shown.append(user1)
        like1.shown.append(user2)
        session.add(like1)
        session.commit()
        return {
            like1: [user1, user2],
            like2: [],
        }

    def add_vk_friends_to_db(self):
        vk_user1 = (
            session.query(models.VKUsers).get(self.data['vk_users'][0].uid))
        vk_user2 = (
            session.query(models.VKUsers).get(self.data['vk_users'][1].uid))
        vk_user3 = (
            session.query(models.VKUsers).get(self.data['vk_users'][2].uid))
        vk_user4 = (
            session.query(models.VKUsers).get(self.data['vk_users'][3].uid))
        vk_user1.friends.append(vk_user3)
        vk_user1.friends.append(vk_user4)
        vk_user3.friends.append(vk_user4)
        session.add_all([vk_user1, vk_user2, vk_user3, vk_user4])
        session.commit()
        return {
            vk_user1: [vk_user3, vk_user4],
            vk_user2: [],
            vk_user3: [vk_user1, vk_user4],
            vk_user4: [vk_user1, vk_user3]
        }

    def add_shown_vk_friendship_to_db(self):
        self.add_vk_friends_to_db()
        vk_friendship1 = session.query(models.VKFriendship).filter(
            models.VKFriendship.left_uid ==
            self.data['vk_users'][0].uid).all()[0]
        vk_friendship2 = session.query(models.VKFriendship).filter(
            models.VKFriendship.left_uid ==
            self.data['vk_users'][0].uid).all()[1]
        user1 = session.query(models.Users).get(self.data['users'][0].id)
        user2 = session.query(models.Users).get(self.data['users'][1].id)
        vk_friendship1.shown.append(user1)
        vk_friendship1.shown.append(user2)
        session.add(vk_friendship1)
        session.commit()
        return {
            vk_friendship1: [user1, user2],
            vk_friendship2: [],
        }

    def test_add_user(self):
        users = session.query(models.Users).all()
        for user in users:
            self.assertIsNotNone(user.registration_timestamp)

    def test_add_vk_user_to_watching(self):
        user1 = session.query(models.Users).get(self.data['users'][0].id)
        user2 = session.query(models.Users).get(self.data['users'][1].id)
        vk_user1 = (
                session.query(models.VKUsers).get(self.data['vk_users'][0].uid))
        vk_user2 = (
                session.query(models.VKUsers).get(self.data['vk_users'][1].uid))
        user1.watch(vk_user1)
        user1.watch(vk_user2)
        user2.watch(vk_user2)
        session.add_all([user1, user2])
        session.commit()
        self.assertIn(vk_user1, user1.watched_vk_users.all())
        self.assertIn(vk_user2, user1.watched_vk_users.all())
        self.assertIn(vk_user2, user2.watched_vk_users.all())

    def test_add_posts(self):
        for post in session.query(models.Posts).all():
            self.assertIsNotNone(post)

    def test_add_liked_posts(self):
        liked_posts = self.add_likes_to_db()
        for post, vk_users in liked_posts.items():
            self.assertEqual(len(post.liked_vk_users), len(vk_users))
            self.assertEqual(vk_users, post.liked_vk_users)
     
    def test_add_shown_likes(self):    
            shown_likes = self.add_shown_likes_to_db()
            for like, users in shown_likes.items():
                self.assertEqual(len(like.shown), len(users))
                self.assertEqual(users, like.shown)

    def test_delete_post(self):
        shown_likes = self.add_shown_likes_to_db()
        post1_id = list(shown_likes.keys())[0].post.id
        post2_id = list(shown_likes.keys())[1].post.id
        self.assertGreater(
            (len(session.query(models.Likes).filter(
                models.Likes.post_id == post1_id).all())),
            0,
        )
        self.assertGreater(
            (len(session.query(models.shown_likes).filter(
                models.shown_likes.c.likes_post_id == post1_id).all())),
            0,
        )
        self.assertGreater(
            len(session.query(models.Likes).filter(
                models.Likes.post_id == post2_id).all()),
            0,
        )
        session.query(models.Posts).filter(models.Posts.id == post1_id).delete()
        session.commit()
        self.assertEqual(
            len(session.query(models.Posts).filter(
                models.Posts.id == post1_id).all()),
            0,
        )
        self.assertEqual(
            len(session.query(models.Likes).filter(
                models.Likes.post_id == post1_id).all()),
            0,
        )
        self.assertEqual(
            len(session.query(models.shown_likes).filter(
                models.shown_likes.c.likes_post_id == post1_id).all()),
            0,
        )
        self.assertGreater(
            len(session.query(models.Likes).filter(
                models.Likes.post_id == post2_id).all()),
            0,
        )

    def test_add_vk_friends(self):
        friendship = self.add_vk_friends_to_db()
        for vk_user, friends in friendship.items():
            self.assertEqual(len(vk_user.all_friendship), len(friends))
            self.assertEqual(len(vk_user.all_friends), len(friends))
            self.assertEqual(friends, vk_user.all_friends)

    def test_add_shown_vk_friendship(self):
        shown_vk_friendship = self.add_shown_vk_friendship_to_db()
        for vk_friendship, users in shown_vk_friendship.items():
            self.assertEqual(len(vk_friendship.shown), len(users))
            self.assertEqual(users, vk_friendship.shown)

    def test_delete_vk_friends(self):
        self.add_shown_vk_friendship_to_db()
        vk_user1 = (
            session.query(models.VKUsers).get(self.data['vk_users'][0].uid))
        vk_user3 = (
            session.query(models.VKUsers).get(self.data['vk_users'][2].uid))
        vk_user4 = (
            session.query(models.VKUsers).get(self.data['vk_users'][3].uid))
        old_count_friends_vk_user1 = len(vk_user1.all_friends)
        old_count_friends_vk_user3 = len(vk_user3.all_friends)
        old_count_friends_vk_user4 = len(vk_user4.all_friends)
        old_count_shown_vk_friendship_vk_user_1 = len(session.query(
            models.shown_vk_friendship).filter(
            models.shown_vk_friendship.c.vk_friendship_left_uid ==
            vk_user1.uid).all())
        vk_user1.friends = [vk_user3]
        session.add(vk_user1)
        session.commit()
        self.assertEqual(
            old_count_friends_vk_user1 - len(vk_user1.all_friends),
            1,
        )
        self.assertEqual(
            old_count_friends_vk_user4 - len(vk_user4.all_friends),
            1,
        )
        self.assertGreater(old_count_shown_vk_friendship_vk_user_1, 0)
        vk_user1.friends.remove(vk_user3)
        session.add(vk_user1)
        session.commit()
        self.assertEqual(
            old_count_friends_vk_user1 - len(vk_user1.all_friends),
            2,
        )
        self.assertEqual(
            old_count_friends_vk_user3 - len(vk_user3.all_friends),
            1,
        )
        self.assertGreater(
            old_count_shown_vk_friendship_vk_user_1,
            len(session.query(
                models.shown_vk_friendship).filter(
                models.shown_vk_friendship.c.vk_friendship_left_uid ==
                vk_user1.uid).all()),
        )


if __name__ == '__main__':
    try:
        unittest.main()
    except:
        pass
    

