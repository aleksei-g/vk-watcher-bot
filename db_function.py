from datetime import datetime, timedelta
from database import session, Base, engine
from sqlalchemy import and_, or_
import models


GET_WATCHING_PER_PERIOD = 24*60*60
DELETE_WATCHING_AFTER = 24*60*60*20
LIMIT_GET_LIKED_POSTS = 10
LIMIT_GET_LAST_FRIENDSHIPS = 10


def init_db():
    Base.metadata.create_all(bind=engine)


def drop_all():
    Base.metadata.drop_all(bind=engine)


def add_user(new_user):
    user = session.query(models.Users).get(new_user.id)
    if not user:
        user = models.Users(
            **{key: val for key, val in new_user.__dict__.items()
               if key in models.Users.__dict__.keys()}
        )
    else:
        user.activate()
    session.add(user)
    session.commit()
    return user


def user_is_active(user):
    user_in_db = session.query(models.Users).get(user.id)
    return user_in_db and user_in_db.is_active()


def deactivate_user(user):
    user_in_db = session.query(models.Users).get(user.id)
    if user_in_db and user_in_db.is_active():
        user_in_db.deactivate()
        vk_users = get_watching_users(user)
        session.add(user_in_db)
        session.commit()
        for vk_user in vk_users:
            del_vk_user_from_watched(user_in_db, vk_user_in_db=vk_user)


def add_vk_user_to_watching(user_in_db, vk_user_in_db):
    user_in_db.watch(vk_user_in_db)
    session.add(user_in_db)
    session.commit()
    return vk_user_in_db


def unwatching_vk_user(user_in_db, vk_user_in_db):
    user_in_db.unwatched(vk_user_in_db)
    session.add(user_in_db)
    session.commit()
    return user_in_db


def get_watching_users(user):
    user_in_db = session.query(models.Users).get(user.id)
    if user_in_db:
        return user_in_db.watched_vk_users.all()


def get_vk_user(vk_user_uid):
    return session.query(models.VKUsers).get(vk_user_uid)


def add_vk_users(new_vk_users):
    vk_users_for_add_to_db = []
    vk_users_in_db = []
    for new_vk_user in new_vk_users:
        vk_user = get_vk_user(new_vk_user.get('id'))
        if not vk_user:
            vk_user = models.VKUsers(
                uid=new_vk_user.get('id'),
                first_name=new_vk_user.get('first_name'),
                last_name=new_vk_user.get('last_name'),
            )
            vk_users_for_add_to_db.append(vk_user)
        vk_users_in_db.append(vk_user)
    if vk_users_for_add_to_db:
        session.add_all(vk_users_for_add_to_db)
        session.commit()
    return vk_users_in_db


def update_friends_vk_user(vk_user_id_db, friends):
    friends_in_db = add_vk_users(friends)
    friends_to_add = list(set(friends_in_db) - set(vk_user_id_db.all_friends))
    friends_to_remove = list(set(vk_user_id_db.all_friends) -
                             set(friends_in_db))
    vk_user_id_db.friends.extend(friends_to_add)
    for friend in friends_to_remove:
        vk_user_id_db.friends.remove(friend)
    session.add(vk_user_id_db)
    session.commit()
    return vk_user_id_db


def update_timestamp_last_check_likes_for_vk_user(vk_user_uid):
    vk_user_in_db = get_vk_user(vk_user_uid)
    if vk_user_in_db:
        vk_user_in_db.last_check_likes_timestamp = datetime.now()
        session.add(vk_user_in_db)
        session.commit()
    return vk_user_in_db


def add_watching(vk_user_info):
    result = {'watching': None, 'is_new': False}
    vk_user_in_db = session.query(models.VKUsers).get(vk_user_info.get('id'))
    if not vk_user_in_db:
        vk_user_in_db = add_vk_users([vk_user_info])[0]
    last_watching = get_last_watching(vk_user_info.get('id'))
    if (last_watching and
            last_watching.online == bool(vk_user_info.get('online'))):
        result['watching'] = last_watching
        return result
    watching = models.Watching(vk_user=vk_user_in_db)
    watching.online = bool(vk_user_info.get('online'))
    watching.last_seen_timestamp = vk_user_info.get('last_seen', {}).get('time')
    watching.platform = vk_user_info.get('last_seen', {}).get('platform')
    watching.timestamp = int(datetime.utcnow().timestamp())
    session.add(watching)
    session.commit()
    result['watching'] = watching
    result['is_new'] = True
    return result


def get_watching_per_period(uid):
    timestamp_over = (int(datetime.utcnow().timestamp()) -
                      GET_WATCHING_PER_PERIOD)
    return (session.query(models.Watching).filter(
        and_(
            models.Watching.uid == int(uid),
            models.Watching.last_seen_timestamp >= timestamp_over)
    ). order_by(models.Watching.timestamp.asc()).all())


def get_last_watching(uid):
    return (session.query(models.Watching).filter_by(uid=uid).
            order_by(models.Watching.timestamp.desc()).first())


def delete_old_watching():
    timestamp_over = int(datetime.utcnow().timestamp()) - DELETE_WATCHING_AFTER
    (session.query(models.Watching).
     filter(models.Watching.last_seen_timestamp <= timestamp_over).delete())
    session.commit()


def del_vk_user_from_watched(user_in_db, vk_user_in_db=None, vk_user_uid=None):
    if vk_user_in_db is None and vk_user_uid:
        vk_user_in_db = session.query(models.VKUsers).get(vk_user_uid)
    else:
        vk_user_uid = vk_user_in_db.uid
    if vk_user_in_db is None:
        return None
    vk_user_full_name = vk_user_in_db.get_full_name()
    unwatching_vk_user(user_in_db, vk_user_in_db)
    if session.query(models.watchers).filter(
                    models.watchers.c.vk_users_uid == vk_user_uid).count() == 0:
        session.query(models.Watching).filter(
            models.Watching.uid == vk_user_uid).delete()
        session.query(models.VKUsers).filter(
            models.VKUsers.uid == vk_user_uid).delete()
        session.commit()
    return vk_user_full_name


def get_list_watching_vk_users():
    query_list_uids = session.query(models.watchers.c.vk_users_uid).distinct()
    return [uid[0] for uid in query_list_uids.all()]


def get_last_liked_posts(uid, limit=LIMIT_GET_LIKED_POSTS):
    return (session.query(models.Likes).filter(
            models.Likes.vk_users_uid == int(uid)).
            order_by(models.Likes.timestamp.desc()).limit(limit)).all()


def get_not_shown_liked_posts(uid, user):
    return (session.query(models.Likes).filter(
        and_(
            models.Likes.vk_users_uid == int(uid),
            ~models.Likes.shown.contains(user))
    ).order_by(models.Likes.timestamp.desc()).all())


def get_not_shown_or_last_liked_posts(uid, user,
                                      limit_last=LIMIT_GET_LIKED_POSTS):
    not_shown_liked_posts = get_not_shown_liked_posts(uid, user)
    last_liked_posts = get_last_liked_posts(uid, limit_last)
    return max(not_shown_liked_posts, last_liked_posts, key=len)


def add_liked_post(vk_user_uid, link_to_post):
    vk_user_in_db = session.query(models.VKUsers).get(vk_user_uid)
    post = session.query(models.Posts).filter(
        models.Posts.link == link_to_post).first()
    if not post:
        post = models.Posts(link=link_to_post)
    if vk_user_in_db not in post.liked_vk_users:
        post.liked_vk_users.append(vk_user_in_db)
        session.add(post)
        session.commit()
    return post


def mark_likes_as_shown(likes, user):
    if not likes:
        return
    for like in likes:
        if user not in like.shown:
            like.shown.append(user)
            session.add(like)
    session.commit()


def delete_post(post_id):
    session.query(models.Posts).filter(models.Posts.id == post_id).delete()
    session.commit()


def delete_old_likes_and_posts():
    datetime_over = datetime.now() - timedelta(seconds=DELETE_WATCHING_AFTER)
    session.query(models.Likes).filter(
        models.Likes.timestamp <= datetime_over).delete()
    session.query(models.Posts).filter(models.Posts.likes == None).delete(
        synchronize_session='fetch')
    session.commit()


def get_last_added_friendships(uid, limit=LIMIT_GET_LAST_FRIENDSHIPS):
    return session.query(models.VKFriendship).filter(
        or_(
            models.VKFriendship.left_uid == int(uid),
            models.VKFriendship.right_uid == int(uid))
    ).order_by(models.VKFriendship.timestamp.desc()).limit(limit).all()


def get_not_shown_friendships(uid, user):
    return session.query(models.VKFriendship).filter(
        and_(
            or_(
                models.VKFriendship.left_uid == int(uid),
                models.VKFriendship.right_uid == int(uid)
            ),
            ~models.VKFriendship.shown.contains(user))
    ).order_by(models.VKFriendship.timestamp.desc()).all()


def get_not_shown_or_last_added_friendships(
        uid, user, limit_last=LIMIT_GET_LAST_FRIENDSHIPS):
    not_shown_friendships = get_not_shown_friendships(uid, user)
    last_added_friendships = get_last_added_friendships(uid, limit_last)
    return max(not_shown_friendships, last_added_friendships, key=len)


def mark_friendship_as_shown(friendships, user):
    if not friendships:
        return
    for friendship in friendships:
        if user not in friendship.shown:
            friendship.shown.append(user)
            session.add(friendship)
    session.commit()


def get_query(user):
    user_in_db = session.query(models.Users).get(user.id)
    if user_in_db:
        return user_in_db.query


def add_params_to_query(user, **kwargs):
    user_in_db = session.query(models.Users).get(user.id)
    query = user_in_db.query
    if not query:
        query = models.Query(user=user_in_db)
    for key, value in kwargs.items():
        setattr(query, key, value)
    session.add(query)
    session.commit()
    return query


def delete_query(user, query=None):
    if query is None:
        user_in_db = session.query(models.Users).get(user.id)
        if user_in_db and user_in_db.query:
            query = user_in_db.query
    if query:
        session.delete(query)
        session.commit()
