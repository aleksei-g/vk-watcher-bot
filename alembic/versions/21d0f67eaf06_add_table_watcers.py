"""add Table watcers

Revision ID: 21d0f67eaf06
Revises: 3178dd02cef0
Create Date: 2017-06-13 21:00:24.361170

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '21d0f67eaf06'
down_revision = '3178dd02cef0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('watchers',
    sa.Column('users_id', sa.Integer(), nullable=True),
    sa.Column('vk_users_uid', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['users_id'], ['users.id'], ),
    sa.ForeignKeyConstraint(['vk_users_uid'], ['vk_users.uid'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('watchers')
    # ### end Alembic commands ###
