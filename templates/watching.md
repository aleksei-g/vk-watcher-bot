_Посещения_
{% for watching in watching_vk_user.watching_list %}*{% if watching.online %}Online{% else %}Offline{% endif %}* {{ convert_timestamp_to_text(watching.last_seen_timestamp, tz_user) }}{% if watching.platform %} {{ watching.platform }}{% endif %}
{% endfor %}
