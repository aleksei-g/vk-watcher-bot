{% if watching_vk_user.get('additional_info') %} _{{watching_vk_user.additional_info}}_
{% endif %}{% if watching_vk_user.get('vk_user') %}{% if not watching_vk_user.vk_user.deactivated %}*{{ watching_vk_user.vk_user.first_name }} {{watching_vk_user.vk_user.last_name}}*
{% if watching_vk_user.watching_list|length > 0 %}{% include "watching.md" %}
{% elif watching_vk_user.liked_list|length > 0 %}{% include "likes.md" %}
{% elif watching_vk_user.friends_list|length > 0 %}{% include "friends.md" %}
{% else %}Нет информации в БД.{% endif %}{% else %}Пользователь удален из ВК!{% endif %}{% endif %}