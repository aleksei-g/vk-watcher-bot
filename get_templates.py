from jinja2 import Environment, FileSystemLoader
from os.path import join, dirname, abspath

template_path = join(dirname(abspath(__file__)), 'templates')
env = Environment(loader=FileSystemLoader(template_path))
greetings_tmpl = env.get_template('greetings.md')
main_tmpl = env.get_template('main.md')

