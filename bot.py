import re
import inspect
from os import getenv
from datetime import datetime
from pytz import timezone
from telebot import TeleBot, apihelper
from telebot.types import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove, \
    InlineKeyboardMarkup, KeyboardButton, InlineKeyboardButton
from flask import Flask, request
from get_templates import main_tmpl, greetings_tmpl
import db_function
from vk_watcher import get_status_vk_users, add_status_vk_users


BOT_TOKEN = getenv('BOT_TOKEN')
BOT_PROXY = getenv('BOT_PROXY')
URL_WEBHOOK = getenv('URL_WEBHOOK')
PORT = getenv('PORT', 5000)
ACCESS_TG_USERS_LIST = [
    int(item) for item in getenv('ACCESS_TG_USERS_LIST', '').split(',')
    if item.strip().isdigit()
]
FMT_STR = '%a, %d %b %H:%M:%S'
TZ_KIROV = timezone('Europe/Moscow')
MAX_LEN_MESSAGE = 4096
TEXT_FOR_DELETED_PART_OF_MESSAGE = '\n...\n'
PLATFORM = {
    1: 'мобильная версия',
    2: 'iPhone',
    3: 'iPad',
    4: 'Android',
    5: 'Windows Phone',
    6: 'Windows 10',
    7: 'cайт',
}
ANSWER_VK_USER_NOT_IN_DB = (
    'ВНИМАНИЕ! Пользователь {vk_user} еще не добавлен в базу данных. '
    'История наблющения недоступна.'
)

if not BOT_TOKEN:
    raise Exception('BOT_TOKEN should be specified')

if BOT_PROXY:
    apihelper.proxy = {'https': BOT_PROXY}
server = Flask(__name__)
bot = TeleBot(BOT_TOKEN)


def is_uin(txt):
    try:
        if int(txt) > 0:
            return True
    except ValueError:
        pass
    return False


def replace_nth(string, old, new, n):
    positions = [m.start() for m in re.finditer(old, string)]
    new_string = string
    if n != 0 and abs(n) <= len(positions):
        if n > 0:
            n = n - 1
        where = positions[n]
        before = string[:where]
        after = string[where:]
        after = after.replace(old, new, 1)
        new_string = before + after
    return new_string


def get_filtered_params_for_func(func, params):
    return {key: value for key, value in params.items()
            if key in inspect.getfullargspec(func).args
            }


def check_user(func):
    def check_user_is_active(message):
        if (ACCESS_TG_USERS_LIST and
                message.from_user.id not in ACCESS_TG_USERS_LIST):
            answer_text = 'Этот функционал бота недоступен для Вас.'
            bot.send_message(message.chat.id, answer_text)
        elif db_function.user_is_active(message.from_user):
            func(message)
        else:
            answer_text = 'Для начала работы с ботом выполните команду /start.'
            bot.send_message(message.chat.id, answer_text)
    return check_user_is_active


def convert_timestamp_to_text(timestamp, tz=None):
    return datetime.fromtimestamp(timestamp, tz).strftime(FMT_STR)


def create_reply_keyboard(button_list):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    keyboard.add(*[KeyboardButton(
        text=button_param[0],
        request_location=button_param[1],
    ) for button_param in button_list])
    return keyboard


def create_general_reply_keyboard(user):
    button_list = []
    if db_function.get_watching_users(user):
        button_list.append(('Пользователи', False))
    button_list.append(('Скрыть клавиатуру', False))
    return create_reply_keyboard(button_list)


def create_add_vk_user_reply_keyboard():
    button_list = [('Отмена', False)]
    return create_reply_keyboard(button_list)


def create_inline_keyboard(button_list, row_width=1):
    keyboard = InlineKeyboardMarkup(row_width=row_width)
    buttons = [InlineKeyboardButton(
        text=button[0],
        callback_data='{}={}'.format(button[1], button[2]))
        for button in button_list]
    keyboard.add(*buttons)
    return keyboard


def create_vk_users_inline_keyboard(vk_users, name_param):
    vk_users.sort(key=lambda vk_user: vk_user.full_name)
    buttons = [(vk_user.full_name, name_param, vk_user.uid)
               for vk_user in vk_users]
    buttons.append(('Отмена', 'cancel', True))
    return create_inline_keyboard(buttons)


def create_select_type_requested_data_inline_keyboard(vk_user_uid):
    buttons = [
        ('Посещения', 'get_watching=', vk_user_uid),
        ('Лайки', 'get_likes=', vk_user_uid),
        ('Друзья', 'get_friends=', vk_user_uid),
        ('Отмена', 'cancel', True)
    ]
    return create_inline_keyboard(buttons)


def parse_status_not_watching_vk_user(vk_user_uid):
    status_list = get_status_vk_users([vk_user_uid])
    if (not len(status_list) or
            (len(status_list) > 0 and status_list[0] is None)):
        return {}
    status = status_list[0]
    return {
        'vk_user': {
            'first_name': status.get('first_name'),
            'last_name': status.get('last_name'),
            'deactivated': status.get('deactivated')
        },
        'watching_list': [
            {
                'last_seen_timestamp': status.get('last_seen', {}).get('time'),
                'online': bool(status.get('online')),
                'platform': PLATFORM.get(status.get('last_seen', {})
                                         .get('platform')),
            },
        ],
    }


def parse_status_watching_vk_user(vk_user):
    watching_per_day = db_function.get_watching_per_period(vk_user.uid)
    if not watching_per_day:
        last_watching = db_function.get_last_watching(vk_user.uid)
        if last_watching:
            watching_per_day = [last_watching]
    return {
        'vk_user': vk_user,
        'watching_list': [
            {
                'last_seen_timestamp': watching.last_seen_timestamp,
                'online': watching.online,
                'platform': PLATFORM.get(watching.platform),
            } for watching in watching_per_day
        ],
    }


def parse_likes_vk_user(vk_user, user):
    likes_vk_user = db_function.get_not_shown_or_last_liked_posts(
        vk_user.uid,
        user,
    )
    return {
        'vk_user': vk_user,
        'liked_list': likes_vk_user,
        'user': user,
    }


def parse_friends_vk_user(vk_user, user):
    friends_vk_user = db_function.get_not_shown_or_last_added_friendships(
        vk_user.uid,
        user,
    )
    return {
        'vk_user': vk_user,
        'friends_list': friends_vk_user,
        'user': user,
    }


def get_vk_user_info_from_db(query, func_if_vk_user, func_if_not_vk_user=None):
    vk_user = db_function.get_vk_user(query.vk_user_uid)
    general_params = {
        'user': query.user,
        'vk_user': vk_user,
        'vk_user_uid': query.vk_user_uid,
    }
    result = {}
    run_func = func_if_vk_user
    if vk_user is None:
        run_func = func_if_not_vk_user
        result['additional_info'] = ANSWER_VK_USER_NOT_IN_DB.format(
            vk_user=query.vk_user_uid)
    if run_func:
        params = get_filtered_params_for_func(run_func, general_params)
        result.update(run_func(**params))
    return result


def add_watching_vk_user(query):
    last_status = (add_status_vk_users([query.vk_user_uid]).
                   get(query.vk_user_uid))
    if last_status:
        vk_user = db_function.add_vk_user_to_watching(
            query.user,
            last_status.vk_user,
        )
        msg = ('Пользователь *{}* добавлен в список наблюдения.'
               .format(vk_user.full_name))
    else:
        msg = 'Пользователь *{}* удален из ВК!'.format(query.vk_user_uid)
    return msg


def cut_answer_to_max_len(message, max_len=MAX_LEN_MESSAGE, reverse=False,
                          caption_line_count=1):
    if len(message) <= max_len:
        return message
    message_list = message.split('\n')
    while (len(message) + len(TEXT_FOR_DELETED_PART_OF_MESSAGE) >
           MAX_LEN_MESSAGE and len(message_list) > caption_line_count + 1):
        if reverse:
            message_list[len(message_list)-1:len(message_list)-0] = []
        else:
            message_list[caption_line_count:caption_line_count + 1] = []
        message = '\n'.join(message_list)
    if reverse:
        message = message + TEXT_FOR_DELETED_PART_OF_MESSAGE
    else:
        message = replace_nth(
            message,
            '\n',
            TEXT_FOR_DELETED_PART_OF_MESSAGE,
            caption_line_count,
        )
    return message


@bot.message_handler(commands=['start', 'help'])
def start_command(message):
    db_function.add_user(message.from_user)
    bot_name = (bot.get_me().first_name if bot.get_me().first_name
                else bot.get_me().username)
    answer = greetings_tmpl.render(bot_username=bot_name)
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown')


@bot.message_handler(commands=['stop'])
def stop_command(message):
    db_function.deactivate_user(message.from_user)
    answer = 'Получена команда /stop. Пользватель деактивирован.'
    return bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['keyboard'])
def keyboard_command(message):
    keyboard = create_general_reply_keyboard(message.from_user)
    answer = 'Показана основная клавиатура'
    return bot.send_message(message.chat.id, answer, reply_markup=keyboard)


@bot.message_handler(commands=['addvkuser'])
@check_user
def add_vk_user_command(message):
    db_function.add_params_to_query(
        user=message.from_user,
        type_query='add_vk_user',
    )
    keyboard = create_add_vk_user_reply_keyboard()
    answer = 'Для добавления пользователя VK в список слежения введите его ID.'
    message = bot.send_message(
        message.chat.id, answer,
        parse_mode='Markdown',
        reply_markup=keyboard,
    )
    return message


@bot.message_handler(commands=['delvkuser'])
@check_user
def del_vk_user_command(message):
    vk_users = db_function.get_watching_users(message.from_user)
    if vk_users:
        db_function.add_params_to_query(
            user=message.from_user,
            type_query='del_vk_user',
        )
        keyboard = create_vk_users_inline_keyboard(vk_users, 'del_vk_user_uid')
        answer = 'Выберите пользователя VK для удаления из списка слежения:'
    else:
        answer = 'Ваш список пользователей VK пуст.'
        keyboard = create_general_reply_keyboard(message.from_user)
    return bot.send_message(
        message.chat.id,
        answer,
        parse_mode='Markdown',
        reply_markup=keyboard,
    )


@bot.message_handler(content_types=['text'],
                     func=lambda message:
                     message.text == 'Пользователи')
@check_user
def push_vk_users_button(message):
    vk_users = db_function.get_watching_users(message.from_user)
    if vk_users:
        answer = 'Выберите пользователя:'
        vk_users_keyboard = create_vk_users_inline_keyboard(vk_users,
                                                            'vk_user_uid')
    else:
        answer = 'Ваш список пользователей VK пуст.'
        vk_users_keyboard = None
    return bot.send_message(
        message.chat.id,
        answer,
        parse_mode='Markdown',
        reply_markup=vk_users_keyboard,
    )


@bot.message_handler(content_types=['text'],
                     func=lambda message:
                     message.text in ['Отмена', 'Скрыть клавиатуру'])
@check_user
def push_cancel(message):
    query = db_function.get_query(message.from_user)
    if query and query.type_query == 'add_vk_user':
        db_function.delete_query(message.from_user, query)
        keyboard = create_general_reply_keyboard(message.from_user)
        answer = 'Отмена добавления пользователя.'
    else:
        keyboard = ReplyKeyboardRemove()
        answer = 'Клавиатура скрыта.'
    return bot.send_message(
        message.chat.id,
        answer,
        parse_mode='Markdown',
        reply_markup=keyboard,
    )


@bot.callback_query_handler(func=lambda call: True
                            and call.data == 'cancel=True')
@check_user
def cancel_callback_inline(call):
    answer = 'Отмена'
    db_function.delete_query(call.from_user)
    return bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=answer,
        parse_mode='Markdown',
    )


@bot.callback_query_handler(func=lambda call: True
                            and call.data.startswith('vk_user_uid='))
@check_user
def vk_user_callback_inline(call):
    vk_user_uid = int(re.sub('vk_user_uid=', '', call.data))
    db_function.add_params_to_query(
        user=call.from_user,
        vk_user_uid=vk_user_uid,
        type_query='select_type_requested_data',
    )
    keyboard = create_select_type_requested_data_inline_keyboard(vk_user_uid)
    answer = 'Выберите действие:'
    return bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=answer,
        parse_mode='Markdown',
        reply_markup=keyboard,
    )


@bot.callback_query_handler(func=lambda call: True
                            and call.data.startswith('get_watching='))
@check_user
def answer_on_get_watching(call):
    query = db_function.add_params_to_query(
        user=call.from_user,
        type_query='get_watching',
    )
    watching_vk_user = get_vk_user_info_from_db(
        query,
        parse_status_watching_vk_user,
        parse_status_not_watching_vk_user,
    )
    answer = main_tmpl.render(
        watching_vk_user=watching_vk_user,
        convert_timestamp_to_text=convert_timestamp_to_text,
        tz_user=TZ_KIROV,
    )
    answer = cut_answer_to_max_len(answer, caption_line_count=2)
    db_function.delete_query(call.from_user)
    return bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=answer,
        parse_mode='Markdown',
    )


@bot.callback_query_handler(func=lambda call: True
                            and call.data.startswith('get_likes='))
@check_user
def answer_on_get_likes(call):
    query = db_function.add_params_to_query(
        user=call.from_user,
        type_query='get_likes',
    )
    likes_vk_user = get_vk_user_info_from_db(query, parse_likes_vk_user)
    answer = main_tmpl.render(watching_vk_user=likes_vk_user)
    answer = cut_answer_to_max_len(answer, reverse=True, caption_line_count=2)
    db_function.delete_query(call.from_user)
    db_function.mark_likes_as_shown(
        likes_vk_user.get('liked_list'),
        query.user,
    )
    return bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=answer,
        parse_mode='Markdown',
    )


@bot.callback_query_handler(func=lambda call: True
                            and call.data.startswith('get_friends='))
@check_user
def answer_on_get_friends(call):
    query = db_function.add_params_to_query(
        user=call.from_user,
        type_query='get_friends',
    )
    friends_vk_user = get_vk_user_info_from_db(query, parse_friends_vk_user)
    answer = main_tmpl.render(watching_vk_user=friends_vk_user)
    answer = cut_answer_to_max_len(answer, reverse=True, caption_line_count=2)
    db_function.delete_query(call.from_user)
    db_function.mark_friendship_as_shown(
        friends_vk_user.get('friends_list'),
        query.user,
    )
    return bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=answer,
        parse_mode='Markdown',
    )


@bot.callback_query_handler(func=lambda call: True
                            and call.data.startswith('del_vk_user_uid='))
@check_user
def del_vk_user_callback_inline(call):
    vk_user_uid = int(re.sub('del_vk_user_uid=', '', call.data))
    query = db_function.add_params_to_query(
        user=call.from_user,
        vk_user_uid=vk_user_uid,
    )
    deleted_vk_user = db_function.del_vk_user_from_watched(
        query.user,
        vk_user_uid=vk_user_uid,
    )
    answer = '*{}* {}'.format(deleted_vk_user, 'удален из списка слежения.')
    db_function.delete_query(call.from_user, query)
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=answer,
        parse_mode='Markdown',
    )
    keyboard = create_general_reply_keyboard(call.from_user)
    return bot.send_message(
        chat_id=call.message.chat.id,
        text='Настройки сохранены.',
        reply_markup=keyboard,
    )


@bot.message_handler(content_types=['text'])
@check_user
def recieved_text(message):
    if not is_uin(message.text):
        return bot.send_message(
            message.chat.id,
            'Необходимо передать UIN пользователя VK!',
        )
    query = db_function.add_params_to_query(
        user=message.from_user,
        vk_user_uid=int(message.text),
    )
    if query.type_query == 'add_vk_user':
        answer = add_watching_vk_user(query)
        keyboard = create_general_reply_keyboard(message.from_user)
        db_function.delete_query(message.from_user, query)
    elif query.type_query == 'del_vk_user':
        answer = 'Выберите пользователя из списка выше'
        keyboard = None
    else:
        keyboard = create_select_type_requested_data_inline_keyboard(
            query.vk_user_uid,
        )
        answer = 'Выберите действие:'
    return bot.send_message(
        message.chat.id,
        answer,
        parse_mode='Markdown',
        reply_markup=keyboard),


@server.route("/{}".format(BOT_TOKEN), methods=['POST'])
def get_message():
    bot.process_new_updates([Update.de_json(request.stream.read()
                                            .decode("utf-8"))])
    return "!", 200


@server.route("/setwebhook")
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url=URL_WEBHOOK)
    return "!", 200


@server.route("/")
def index():
    return "vk-watcher-bot", 200


if URL_WEBHOOK:
    server.run(host="0.0.0.0", port=PORT)
    server = Flask(__name__)
else:
    bot.polling(none_stop=True)
