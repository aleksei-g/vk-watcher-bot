from database import Base, engine

if __name__ == '__main__':
    answer = input('Очистить базу данных?\n'
                   'Продолжить? [Y,N]: ')
    if answer.lower() == 'y':
        Base.metadata.reflect(engine)
        Base.metadata.drop_all(engine)
        print('База данных очишена!')
    else:
        print('Отмена')
